# 介绍
致力于打造结构简明,易于扩展的模块式golang开发框架，基于gin框架

## 项目地址
[https://gitee.com/plugins-gin](https://gitee.com/plugins-gin)

## 目录
- [序言](/guide/monibuca-analyse)
- [构建启动实例](/guide/build-entrace)
- [插件模块开发](/guide/plugin-create-guide)