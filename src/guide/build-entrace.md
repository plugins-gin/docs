# 构建启动实例


## 使用方法演示

- main.go 引入模块

```go
package main

import (
	. "gitee.com/plugins-gin/engine"  //基础模块
	_ "gitee.com/plugins-gin/gateway" //路由模块
	_ "gitee.com/plugins-gin/plugin1" //插件模块
	_ "gitee.com/plugins-gin/plugin2" //插件模块
)

func main() {
	Run("config.toml")
	select {}
}
```

- config.toml 插件模块配置
```toml
[gateway]
ListenAddr = ":8081"
[module1]
[module2]
```

- 运行
```bash
$ go run main.go
2020/10/26 16:44:35 install plugin gateway
2020/10/26 16:44:35 install plugin module1
2020/10/26 16:44:35 install plugin module2
2020/10/26 16:44:35 Ⓜ start monibuca 
2020/10/26 16:44:35 server gateway start at  :8081
```


## 效果展示
- http://localhost:8081/

![An image](./static/bashboard-show.png)



