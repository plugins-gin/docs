# 序言

致力于打造结构简明,易于扩展的golang开发框架，基本框架思想起源于[Monibuca](https://monibuca.com/),结合[gin](https://gin-gonic.com/)框架，实现融合。


## Monibuca 基础模块分析

### monica 安装器模块
实例管理器，实为安装引导，提供ui界面，引导安装，根据所选模块及设置模块配置，生成一个新的正式的应用程序用于运行，

![An image](./static/steps.png)

#### 构造目标应用目录结构

```$xslt
.
├── config.toml    # 配置文件
└── main.go        # 入口文件
```
main.go入口文件示例
```gherkin
package main

import (
	. "github.com/Monibuca/engine/v2"
	_ "github.com/Monibuca/plugin-gateway"
	_ "github.com/Monibuca/plugin-jessica"
	_ "github.com/Monibuca/plugin-logrotate"
	_ "github.com/Monibuca/plugin-record"
	_ "github.com/Monibuca/plugin-rtmp"
	_ "github.com/Monibuca/plugin-rtsp"
)

func main() {
	Run("config.toml")
	select {}
}
```

::: tip 执行go mod init 需开启GO111MODULE
```bash
# 开启GO111MODULE
go env -w GO111MODULE=on
# 设置gocache为src目录
go env -w GOMODCACHE=$GOPATH/src
```
:::


### engine 框架基础模块

**config.go**
* 定义通用插件配置
* 通用插件安装程序


**hook.go 事件勾子**
* AuthHooks            
* OnPublishHooks       
* OnSubscribeHooks     
* OnUnSubscribeHooks   
* OnDropHooks          
* OnSummaryHooks       
* OnStreamClosedHooks  

**logger.go 公共日志函数**
* Print()
* Printf()
* Println()

**main.go 启动函数**
* Run() 启动服务函数


### 通用 应用插件模块   
* init()初始化函数，实现插件注册到插件配置器
* run()函数， 项目运行时通过插件配置器的配置，依次调用各模块的run()函数，名称可能不是run(),具体参见init集数中的插件注册方法， run函数可以绑定自己的路由，监听http请求模块动态资源路径


### 特殊 应用插件模块plugin-gateway 实现插件模块路由
* 沿用通用应用插件模块的init, 和run
* 在run()函数中增加了监听端口，开启http服务，
* 监听 plugin/ 请求插件模块静态资源，通过插件配置器的路径配置，取出对应静态文件渲染给前端


## Monibuca 模块功能流程
![An image](./static/module-flow.jpg)


