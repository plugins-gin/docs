# 插件模块开发


## 模块目录结构
```sybase
.
├── main.go
└── ui
    └── dist
        └── index.html
```

## 文件说明
- ui/dist 模块的ui界面静态资源

- main.go 模块初始化，配置侦听动态数据请求

```go
package other2

import (
	. "gitee.com/plugins-gin/engine"
	"github.com/gin-gonic/gin"
)

type ModuleConfig struct{}

var config = new(ModuleConfig)

func init() {
	plugin := &PluginConfig{
		Name:   "module2",
		Type:   PLUGIN_APP,
		Config: config,
		Run:    run,
	}
	InstallPlugin(plugin)
}

func run() {
	GinR.GET("/module2/ping", ping)
}

func ping(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "plugin2 pong",
	})
}

```

## main.go 函数说明

- init()函数
    初始化函数，以 “_” 开头的方式引入。 引入时自动执行，运行插件的安装，声明插件类型，插件名称，初始配置，运行函数。
- run()函数
    模块启动函数，名称不固定，可随init()函数中声明做改动。
    
