# 开始


## 使用方法演示

. main.go 引入模块

```sybase
package main

import (
	. "gitee.com/plugins-gin/engine"
	_ "gitee.com/plugins-gin/gateway"
	_ "gitee.com/plugins-gin/plugin1"
	_ "gitee.com/plugins-gin/plugin2"
)

func main() {
	Run("config.toml")
	select {}
}
```

. config.toml 插件模块配置
```sybase
[gateway]
ListenAddr = ":8081"
[module1]
[module2]
```

. 运行
```sybase
go run dev
```


